# Ionic Funai Template

How to run ?

1. Clone the Repository.
2. cd Funai-multi-platform
3. npm install -g ionic cordova
4. ionic serve

How to Build ?

See [Ionic 3 Documentation](http://ionicframework.com/docs/v1/guide/building.html) for Building Android and iOS App.
