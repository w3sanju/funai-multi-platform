import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProductListProvider } from '../../providers/product-list/product-list';
import { CategorySelectionPage } from '../category-selection/category-selection';

import { RegisterPage } from './../register/register';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  product:any;
  location:{
    city:string,
    state:string
  }

  public langName: string = "US spanish";

    countryName: any;
    currentCountry;
    countryClass: any;


    selectOptions: any = {
      title: 'Language',
      mode: 'md',
      cssClass: 'my-class'
    };
    vals: any;

      lang: any;
      langList: Array<{ lgName: string, lgID: number, flag: string }>;

      // tabsPage = TabsPage;
      productPics: Array<{ title: string, modelPic: string, prod_num_id: string }>;

  constructor(
    public navCtrl: NavController,
    private productListProvider:ProductListProvider) {

  }

  ionViewWillEnter(){

    sessionStorage.setItem('comp_name','1');

    if (!localStorage.getItem('lang')) {
      // sessionStorage.setItem('lang', '1');
      console.log('no lang');
      this.countryClass = 'us';
    }
    else {
      console.log('got lang');
    }


    this.productPics = [];

    this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
        { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
        { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
        { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
        { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

    if (sessionStorage.getItem("lang") !== null && sessionStorage.getItem("lang") !== "") {
      this.lang = sessionStorage.getItem('lang');
      this.countryName = sessionStorage.getItem('lang');
      this.countryClass = sessionStorage.getItem('langClass');
      console.log(this.countryName);
    }
    else {
      this.lang = sessionStorage.setItem('lang', '1');
      this.countryClass = 'us';
      sessionStorage.setItem('langClass','us');
      this.countryName = 1;
      console.log(this.countryName);
    }



    this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=109')  .subscribe(success => {
        // this.product = weather.current_observation;
        console.log(success);
        if (success) {
          this.product = true;
          for (var i = 0; i < success.model_info.length; i++) {
            this.productPics.push({ title: success.model_info[i].model_number, modelPic: success.model_info[i].img, prod_num_id: success.model_info[i].num_id });
          }
        }
        else
        {
            this.product = false;
        }
      });
    }

    setLang(val: any) {
    console.log(val);
    sessionStorage.setItem('lang', val);
    localStorage.setItem('lang', val);

    // this.navCtrl.setRoot(HomePage);

    this.currentCountry = this.langList.filter(c => {
      if (c.lgID == val) {
        return c;
      }
    });
    console.log(this.currentCountry[0].flag);
    this.countryClass = this.currentCountry[0].flag;
    sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    //this.navCtrl.push(this.navCtrl.getActive().component);
  }

  goAnOtherPage(tokenNumber, prod_num_id, bnrImg) {
    localStorage.setItem('token_array', tokenNumber);
    localStorage.setItem('prod_num_id', prod_num_id);
    localStorage.setItem('bnrImg', bnrImg);

    this.navCtrl.push(CategorySelectionPage, { 'token_array': tokenNumber });
  }

  pushRegister(){
    this.navCtrl.setRoot(RegisterPage);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.navCtrl.setRoot(this.navCtrl.getActive().component);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


}
