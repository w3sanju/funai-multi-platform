// import { Component } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Tabs} from 'ionic-angular';
// import {ViewChild } from '@angular/core';
import {HomePage } from './../home/home';
import { RegisterPage } from './../register/register';
import { DownloadPage} from './../download/download';
import { TroubleshootPage} from './../troubleshoot/troubleshoot'
import 'rxjs/add/operator/switchMap';
// import { Headers, RequestOptions } from '@angular/http';
// import { HttpService} from '../../services/http.service';
import { ProductListProvider } from '../../providers/product-list/product-list';


@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html'
  })
  export class TabsPage {
    data: any;
    currentCountry;
    countryClass = sessionStorage.getItem('langClass');
    hideHeader: any;

    constructor(
      // private _HttpService: HttpService,
      // private httpService: HttpService,
      private productListProvider:ProductListProvider,
      public navCtrl: NavController,
      public navParams: NavParams
      // private router: Router
    ) { }
    // this tells the tabs component which Pages
    // should be each tab's root Page
    activeTab = parseInt(localStorage.getItem('activeTab'));
    homePage = HomePage;
    tab1Root = DownloadPage;
    tab2Root = TroubleshootPage;

    lang = sessionStorage.getItem('lang');
    langList: Array<{ lgName: string, lgID: number, flag:string }>;

    @ViewChild('solutionTabs') tabRef: Tabs;

    ionViewDidEnter() {
      this.tabRef.select(this.activeTab);
      console.log(this.activeTab);
    }


    productPics: Array<{title: string, modelPic: string}>;
    ionViewWillEnter() {
    
    if(this.navParams.get('hideHeader')== true)
    {
      this.hideHeader = true;
    }
    else
    {
      this.hideHeader = false;
    }
      this.onSubmit();
      this.productPics = [];
      this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
      { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
      { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
      { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
      { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

      console.log(this.lang);

    }

    setLang(val: any) {
      console.log(val);
      sessionStorage.setItem('lang', val);
      this.navCtrl.setRoot(this.navCtrl.getActive().component);

      this.currentCountry = this.langList.filter(c => {
        if (c.lgID == val) {
          return c;
        }
      });
      console.log(this.currentCountry[0].flag);
      this.countryClass = this.currentCountry[0].flag;
      sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    }

    onSubmit() {

      var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');
      console.log(tokenId);

      this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=1')  .subscribe(success => {
      // return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
      //   (success) => {
          if (success) {
            this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
          } else {
            console.log('Error in success');
          }
        })
    }


    // constructor(private navCtrl:NavController) {
    //   //console.log(this.activeTab);
    // }

    pushBack() {
      this.navCtrl.pop();
      // this.navCtrl.setRoot(HomePage);

    }
    pushHome() {
      // this.navCtrl.push(HomePage);
      this.navCtrl.setRoot(HomePage);
    }
    pushRegister() {
      this.navCtrl.setRoot(RegisterPage);
    }




  }
