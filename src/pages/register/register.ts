import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { ProductListProvider } from './../../providers/product-list/product-list';
import * as $ from "jquery";
// declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage implements OnInit {

stateListing: any;
tokyoDay: any;

  registerForm: FormGroup;
  firstName: any;
  lastName: any;
  emailAddress: any;
  verifyEmail: any;
  address1: any;
  address2: any;
  city: any;
  country: any;
  state: any;
  postalCode: any;
  mobileNumber: any;
  landLineNumber: any;
  choosePrinter: any;
  serialNumber: any;
  purchaseMonth: any;
  purchaseDay: any;
  purchaseYear: any;
  formData : any;
  sucessMessage : any;
  privacyPolicyoverlayHidden: boolean = true;
  serialNumoverlayHidden: boolean = true;
  whyRegPrinteroverlayHidden: boolean = true;
  feedbackoverlayHidden: boolean = true;
  subRadioGroup: boolean = false;
  subRadioGroupSecond: boolean = false;
  feedbackFormoverlayHidden: boolean = true;
  yesThankoverlayHidden: boolean = true;
  months: any;
  days: any;
  years: any;


  productH1: string;
  productH2: string;
  productTxt1: string;
  productTxt2: string;
  lang: any;

  countryListing: any;
  printerModelNumber: any;

  countryName: any;
  currentCountry;
  countryClass = sessionStorage.getItem('langClass');

  printerOptions: any = {
    title: 'Choose Printer',
    mode: 'md',
    cssClass: 'my-class'
  };

  MonthOptions: any = {
    title: 'Purchase Month',
    mode: 'md',
    cssClass: 'my-class'
  }

  DayOptions: any = {
    title: 'Purchase Day',
    mode: 'md',
    cssClass: 'my-class'
  }

  countryOptions: any = {
    title: 'Choose Country',
    mode: 'md',
    cssClass: 'my-class'
  }

  stateOptions: any = {
    title: 'Choose State',
    mode: 'md',
    cssClass: 'my-class'
  }

  tokyoTime: any;

  constructor(
    //private _HttpService: HttpService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    //private httpService: HttpService,
    public alertCtrl: AlertController,
    private productListProvider:ProductListProvider,
  ) {
    this.tokyoTime = this.calculateTime('0');
  }

  langList: Array<{ lgName: string, lgID: number, flag: string }>;

myDate: any;

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.countryList();
    this.printerModel();
    this.yearList();
    this.dayList();
    // this.subRadioValue = false;
    this.feedbackoverlayHidden = true;

    this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
    { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
    { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
    { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
    { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

    if (sessionStorage.getItem("lang") == null && sessionStorage.getItem("lang") == "") {
      this.lang = 1;
      this.countryName = 1;
      sessionStorage.setItem('lang', '1');
    }
    else {
      this.lang = sessionStorage.getItem('lang');
      this.countryName = sessionStorage.getItem('lang');
      console.log(this.countryName);
    }

  }

  selectOptions: any = {
    title: 'Language',
    mode: 'md',
  };


  createFormControls(){
    this.firstName = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
    this.lastName = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
    this.emailAddress = new FormControl(null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]);
    this.verifyEmail = new FormControl (null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]);
    this.address1 = new FormControl (null, Validators.required);
    this.address2 = new FormControl ('');
    this.city = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
    // this.country = new FormControl ({ value: 0 });
    this.country = new FormControl ('', [Validators.required, Validators.pattern("^[0-9]{1,5}$")]);
    this.state = new FormControl (null, [Validators.required, Validators.pattern("^[0-9]{2,5}$")]);
    this.postalCode = new FormControl (null, [Validators.required, Validators.pattern("^[0-9]{4,8}$")]);
    this.mobileNumber = new FormControl (null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]);
    this.landLineNumber = new FormControl ('');
    this.choosePrinter = new FormControl ('');
    // this.serialNumber = new FormControl ('J', [Validators.required, Validators.pattern('^((J-?)|0)?[0-9]{4,10}$'), Validators.minLength(4), Validators.maxLength(10)]);
    // this.serialNumber = new FormControl ('J', [Validators.required, Validators.pattern('^[Jj][0-9]{4,10}$')]);
    this.serialNumber = new FormControl ('', [Validators.required, Validators.pattern('^[0-9]{4,10}$')]);
    this.purchaseMonth = new FormControl (null, Validators.required);
    this.purchaseDay = new FormControl (null, Validators.required);
    this.purchaseYear = new FormControl (null, Validators.required);
    this.tokyoDay = new FormControl ('');
  }

  createForm() {
    this.registerForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
      emailAddressValid:this.fb.group({
        emailAddress: this.emailAddress,
        verifyEmail: this.verifyEmail,
      },),
      address1: this.address1,
      address2: this.address2,
      city: this.city,
      country: this.country,
      state: this.state,
      postalCode: this.postalCode,
      mobileNumber: this.mobileNumber,
      landLineNumber: this.landLineNumber,
      choosePrinter: this.choosePrinter,
      serialNumber: this.serialNumber,
      purchaseMonth: this.purchaseMonth,
      purchaseDay: this.purchaseDay,
      purchaseYear: this.purchaseYear,
      tokyoDay: this.tokyoDay
    });
  }

  ionViewDidLoad() {
    this.monthsList();

    console.log('ionViewDidLoad RegisterPage');
  }

  countryList(){
    this.productListProvider.getProducts('countries.json')  .subscribe(success => {
      this.countryListing = success;
    });

    // return this.productListProvider.get('countries.json').then((success) => {
    //   //console.log(success);
    //   this.countryListing = success;
    //   //console.log(this.countryListing)
    // }).catch(
    //     (err)=> {
    //       console.log(err);
    // })

  }

  stateList(event){
    console.log(event);
    if(event != 0)
    {
      this.productListProvider.getProducts('regions.json?country=' + event)  .subscribe(success => {
        this.stateListing = success;
      });
    }
  }

  onSubmit(){

    // $('#sucessMessage').text("Loading.... ");
          // $('body').css('background-color','blue');
          if (this.registerForm.valid) {
            var data = this.registerForm.value;
            console.log(data);
            console.log($('#emailAddress').val());
            var verified_email = $('#emailAddress').val();

            // let dD = new Date();
            // var offset = 0;
            // let ndD = new Date(data.tokyoDay.getTime() + (3600000 * offset));
            // console.log(new Date(data.tokyoDay.getTime()))

            var strDate = data.tokyoDay;
            console.log(strDate.substring(8, 10));

          var postdata = {
                first_name: data.firstName,
                last_name: data.lastName,
                comp_id: sessionStorage.getItem('comp_name'),
                email: verified_email,
                address_first: data.address1,
                address_second: data.address2,
                city: data.city,
                country: data.country,
                state: data.state,
                postal_code: data.postalCode,
                mobile_number: data.mobileNumber,
                landline_number: data.landLineNumber,
                choosePrinter: data.choosePrinter,
                serial_number: 'J' + data.serialNumber,
                serial_start: 'S',
                purchase_month: data.purchaseMonth.substring(5, 7),
                purchase_date: data.purchaseDay.substring(8, 10),
                purchase_year: data.purchaseYear,
                product_model_nums_id: data.choosePrinter
            };
            console.log(postdata);
            this.registerForm.reset();
           $.ajax({
              url: 'http://124.30.44.230/funaihelpver1/api/ver1/productregistration',
              method:'POST',
              crossDomain: true,
              beforeSend:function(xhr){
                xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
              },
              async:true,
              data:postdata,
              success: function(result) {
                  console.log(result);
                  // this.sucessMessage = "Send Mail successfully.";
                  $('#sucessMessage').text(result.response_message);
              },
              error: function(request,msg,error) {
                  // this.sucessMessage = "Send Mail - Server Request Error !";
                  $('#sucessMessage').text(request.response_message);
                  console.log('Error code: '+error);
                  console.log('Error message: '+msg);
              }
          });
        }

    // if (this.registerForm.valid) {
    //   var data = this.registerForm.value;
    //   data= {
    //     "first_name": data.firstName,
    //     "last_name": data.lastName,
    //     "email": data.emailAddress,
    //     "verifyEmail": data.verifyEmail,
    //     "address_first": data.address1,
    //     "address_second": data.address2,
    //     "city": data.city,
    //     "country": data.country,
    //     "state": data.state,
    //     "postal_code": data.postalCode,
    //     "mobile_number": data.mobileNumber,
    //     "landline_number": data.landLineNumber,
    //     "choosePrinter": data.choosePrinter,
    //     "serial_number": data.serialNumber,
    //     "purchase_month": data.purchaseMonth,
    //     "purchase_date": data.purchaseDay,
    //     "purchase_year": data.purchaseYear,
    //   }
    //   this.productListProvider.postRegister('productregistration' , data)  .subscribe(success => {
    //     console.log(success);
    //     if (success.status == 200) {
    //       this.registerForm.reset();
    //       this.sucessMessage = "Product Registration successful."
    //     }
    //   })
    // }
  }

  privacyPolicy() {
    this.privacyPolicyoverlayHidden = false;
  }
  serialNo(){
    this.serialNumoverlayHidden = false;
  }
  whyRegPrinter(){
    this.whyRegPrinteroverlayHidden = false;
    console.log('Why Register');
  }
  feedback(){
    this.feedbackoverlayHidden = false;
  }
  close(){
    this.privacyPolicyoverlayHidden = true;
    this.serialNumoverlayHidden = true;
    this.whyRegPrinteroverlayHidden = true;
    this.feedbackoverlayHidden = true;
    this.yesThankoverlayHidden = true;
  }
  feedbackFormOpen(){
    this.feedbackFormoverlayHidden = false;
    this.feedbackoverlayHidden = true;
  }
  thankYou(){
    this.feedbackFormoverlayHidden = true
  }
  subRadio(){
    this.subRadioGroup = !this.subRadioGroup;
  }
  subRadioSecond(){
    this.subRadioGroupSecond = !this.subRadioGroupSecond;
  }
  yesThankYou(){
    this.yesThankoverlayHidden = false;
  }

  printerModel(){
    this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=1')  .subscribe(success => {

      //console.log(success.model_info.model_number);
      this.printerModelNumber = success.model_info;
      //console.log(this.printerModelNumber)
    })
  }

  monthsList(){
    return  this.productListProvider.getProducts('months?lang_to=en')  .subscribe(success => {
    //return this.httpService.get('months?lang_to=en').then((success) => {
      //console.log(success);
      this.months = success;
      // console.log(this.months)
    })
  }

  dateList(){
    return  this.productListProvider.getProducts('date?lang_to=en')  .subscribe(success => {
      //console.log(success);
      this.months = success;
      // console.log(this.months)
    });
  }

  dayList(){
    var daysListing = [];
    this.days = daysListing;
    for (let i= 1; i <= 31; i++) {
      daysListing.push(i);
    }
  }

  yearList(){
    let yearsListing = [];
    this.years = yearsListing;
    var currentYear = new Date().getFullYear();
    for (let i= 2000; i <= currentYear; i++) {
      yearsListing.push(i);
    }
  }



  setLang(val: any) {
    //console.log(val);
    sessionStorage.setItem('lang', val);
    //sessionStorage.setItem('langClass', val);
    this.navCtrl.push(this.navCtrl.getActive().component);

    this.currentCountry = this.langList.filter(c => {
      if (c.lgID == val) {
        return c;
      }
    });
    //console.log(this.currentCountry[0].flag);
    this.countryClass = this.currentCountry[0].flag;
    sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    console.log(this.countryClass);
  }

  getContent() {
    console.log(sessionStorage.getItem('lang'));
    this.productListProvider.getProducts('static_content?lang_id=' + this.lang )  .subscribe(success => {

          (success) => {
            if (success) {
              this.productH1 = success.product_downloads_heading;
              this.productH2 = success.product_resolution_center_heading;

              this.productTxt1 = success.product_help_message_text;
              this.productTxt2 = success.resolution_product_help_message_text;
            } else {
              console.log('Error in success');
            }
          }

        });
      }


      pushRegister(){
        this.navCtrl.setRoot(RegisterPage);
      }

      calculateTime(offset: any) {
    // create Date object for current location
    let d = new Date();

    // create new Date object for different city
    // using supplied offset
    let nd = new Date(d.getTime() + (3600000 * offset));

    console.log(nd.getDate());

    return nd.toISOString();
    // return nd.getDate();
  }

}
