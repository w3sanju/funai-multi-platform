import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
// import { HttpService} from '../../services/http.service';
import{ Troubleshoot_3Page } from '../troubleshoot-3/troubleshoot-3';
import { ProductListProvider } from '../../providers/product-list/product-list';
import { DownloadPage} from './../download/download';
import {HomePage } from './../home/home';
import { RegisterPage } from './../register/register';
import { CategorySelectionPage } from './../category-selection/category-selection';
import { TroubleshootPage} from './../troubleshoot/troubleshoot'

/**
 * Generated class for the Troubleshoot_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-troubleshoot-2',
  templateUrl: 'troubleshoot-2.html',
})
export class Troubleshoot_2Page {

  productLevel2List: Array<{title: string, level_id3: string}>;
  new_title: string = this.navParams.get('new_title');
  parent_Title1: string = this.navParams.get('parent_Title');

  // lang = sessionStorage.getItem('lang');
  menuBackBtn: Boolean = true;
  bannerPics: Array<{title: string, modelPic: string}>;

  langList: Array<{ lgName: string, lgID: number, flag:string }>;
  lang: any;
  currentCountry;
  countryClass = sessionStorage.getItem('langClass');
  countryName: any;

  constructor(
    // private httpService: HttpService,
    private productListProvider:ProductListProvider,
    public navCtrl: NavController, public navParams: NavParams)
  { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Troubleshoot_2Page');
  }

ngOnInit(): void {

  this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
  { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
  { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
  { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
  { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];


  if (sessionStorage.getItem("lang") == null && sessionStorage.getItem("lang") == "") {
    this.lang = 1;
    this.countryName = 1;
    sessionStorage.setItem('lang', '1');
  }
  else {
    this.lang = sessionStorage.getItem('lang');
    this.countryName = sessionStorage.getItem('lang');
    console.log(this.countryName);
  }

  console.log(this.navParams.get('new_id'));
  this.productLevel2List = [];
  this.onSubmit();
  this.bannerPics = [];
  this.productBanner();
}

selectOptions: any = {
  title: 'Language',
  mode: 'md',
};

setLang(val: any) {
  console.log(val);
  sessionStorage.setItem('lang', val);
  // this.navCtrl.setRoot(this.navCtrl.getActive().component);


  this.currentCountry = this.langList.filter(c => {
    if (c.lgID == val) {
      return c;
    }
  });
  console.log(this.currentCountry[0].flag);
  this.countryClass = this.currentCountry[0].flag;
  sessionStorage.setItem('langClass', this.currentCountry[0].flag);
}

// http://124.30.44.230/funaihelpver1/api/ver1/secondlevel.json?firstlevel=4&lang=1
onSubmit() {

  // var tokenId = localStorage.getItem('token_array');
  // var tokenId = this.navParams.get('token_array');
  console.log(this.navParams.get('ref_id'));

// this.new_title = this.navParams.get('new_title');

// return this.httpService.get('secondlevel.json?firstlevel=' + this.navParams.get('ref_id') + '&lang=' + this.lang).then(
// this.productListProvider.getProducts('thirdlevel.json?secondlevel=2&lang=1')  .subscribe(success => {

this.productListProvider.getProducts('secondlevel.json?firstlevel=' + this.navParams.get('ref_id') + '&lang=' + this.lang)  .subscribe(success => {

  // return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
    // (success) => {
      console.log(success.model_number);
      if (success) {
        console.log(success);
        for (var i = 0; i < success.length; i++) {
console.log( success[i]);
          this.productLevel2List.push({ title: success[i].level_details, level_id3: success[i].level_id });
        }
console.log(this.productLevel2List);
        // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
      } else {
        console.log('Error in success');
      }
    });
}

onclick(new_id, level_id3, new_title){
  this.navCtrl.push(Troubleshoot_3Page,{'new_id': new_id, level_id3: level_id3, 'new_title': new_title, 'parent_Title2': this.parent_Title1});
}

pushBack() {
  this.navCtrl.pop();
}

productBanner() {

  var tokenId = localStorage.getItem('token_array');
  // var tokenId = this.navParams.get('token_array');
  console.log(tokenId);

  this.productListProvider.getProducts('productlist.json?compid=1&prodid=1&lang=1')  .subscribe(success => {
  // return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
  //   (success) => {
      if (success) {
        this.bannerPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
      } else {
        console.log('Error in success');
      }
    })
  }

  gotoDownld()
  {
    this.navCtrl.push(DownloadPage);
  }
  gotoTrob()
  {
    this.navCtrl.push(TroubleshootPage);
  }

  pushHome() {
    this.navCtrl.setRoot(HomePage);
  }

  gotoBack()
  {
    this.navCtrl.pop();
  }
  pushRegister() {
     this.navCtrl.setRoot(RegisterPage);
  }
  pushCategory() {
     this.navCtrl.push(CategorySelectionPage);
  }

}
