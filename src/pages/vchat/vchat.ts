import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the VchatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

 import { FormGroup, FormControl } from '@angular/forms';
 import * as $ from "jquery";
 // declare var jquery:any;
 declare var $ :any;
 import { AlertController } from 'ionic-angular';
 import { RegisterPage } from './../register/register';

@Component({
  selector: 'page-vchat',
  templateUrl: 'vchat.html',
})
export class VchatPage {


  levelValue = localStorage.getItem('levelTo5Value');

  productListing:any;

  surveyForm: FormGroup;
  mySurvey: any;
  mySurveyChildOne: any;
  survey:any;

  feedbackIntro: boolean = true;


  feedbackoverlayHidden: boolean = true;
  feedbackFormoverlayHidden: boolean = true;
  thankYouMsgHidden: boolean = true;

  productNameId = localStorage.getItem('prod_num_id');

  deviceIs: any;
  data: any;
  tabVisibl: any;
  all: any;

  show: any;
  showSib: any;


    faqs :any;


    showAlertMessage: boolean = true;
shouldLeave: boolean = false;

hidePopup: boolean = false;


lang: any;
langList: Array<{ lgName: string, lgID: number, flag: string }>;

public langName: string = "US spanish";

  countryName: any;
  currentCountry;
  countryClass: any;


  selectOptions: any = {
    title: 'Language',
    mode: 'md',
    cssClass: 'my-class'
  };

  tickMarkSurvey: any;
lavelID = localStorage.getItem('level_id3New');
 // levelValue = localStorage.getItem('levelTo5Value');
 //  productNameId = localStorage.getItem('prod_num_id');
  TabsName: any = localStorage.getItem('activeTab');

  parent_Title3 = this.navParams.get('parent_Title2');
  new_title3 = this.navParams.get('new_title3');
  title_4 = this.navParams.get('title_4');


  constructor(public navCtrl: NavController, public navParams: NavParams,
  public alertCtrl: AlertController) {
  }

  ngOnInit(){

    if(this.TabsName == 0)
    {
      this.TabsName = 'download';
    }
    else
    {
      this.TabsName = 'resolution';
    }


      if (!localStorage.getItem('lang')) {
        // sessionStorage.setItem('lang', '1');
        console.log('no lang');
        this.countryClass = 'us';
      }
      else {
        console.log('got lang');
      }


      this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
          { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
          { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
          { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
          { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

      if (sessionStorage.getItem("lang") !== null && sessionStorage.getItem("lang") !== "") {
        this.lang = sessionStorage.getItem('lang');
        this.countryName = sessionStorage.getItem('lang');
        this.countryClass = sessionStorage.getItem('langClass');
        console.log(this.countryName);
      }
      else {
        this.lang = sessionStorage.setItem('lang', '1');
        this.countryClass = 'us';
        sessionStorage.setItem('langClass','us');
        this.countryName = 1;
        console.log(this.countryName);
      }

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad VchatPage');
  }

  ionViewCanLeave()
  {

    this.feedbackoverlayHidden = false;
    this.feedbackIntro = false;
    return this.shouldLeave;

  }



      feedbackFormOpen(value){
      this.feedbackFormoverlayHidden = false;
      this.tickMarkSurvey = value;
    }

      yesThankYou(value){

        this.feedbackIntro = true;

      this.feedbackoverlayHidden = true;

      this.feedbackFormoverlayHidden = true;

      this.thankYouMsgHidden = false;
      this.tickMarkSurvey = value;
      var postdata = {
        channel: "faqs",
        feedback_id: 0,
        num_id: this.productNameId,
        third_level: this.lavelID,
        status: this.tickMarkSurvey,
        page_name: this.TabsName,
        device_id: this.deviceIs,
      };
      console.log(postdata);
      $.ajax({
        url: 'http://124.30.44.230/funaihelpver1/api/ver1/survey',
        method:'POST',
        crossDomain: true,
        beforeSend:function(xhr){
          xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        },
        async:true,
        data:postdata,
        success: function(result) {
          console.log(result);
        },
        error: function(request,msg,error) {
        }
      });
      setTimeout(()=>{
        $('.my-overlay').fadeOut();
  this.hidePopup = true;
        this.navCtrl.pop();
        this.shouldLeave = true;
      }, 2000);

    }

      // yesThankYou(){
      //   // this.feedbackoverlayHidden = true;
      //   this.feedbackoverlayHidden = false;
      //   this.thankYouMsgHidden = true;
      //   this.navCtrl.pop();
      //   this.shouldLeave = true;
      // }

      form = new FormGroup({
        survey: new FormControl(),
        mySurveyChildOne: new FormControl(),
      });

      putSurvey(){
      if (this.mySurvey == '3' && this.mySurvey == '4') {
        this.mySurveyChildOne = this.mySurveyChildOne;
      } else {
        this.mySurveyChildOne = this.levelValue;
      }
      console.log(this.mySurveyChildOne);

      this.feedbackoverlayHidden = true;
      this.thankYouMsgHidden = false;
      this.feedbackFormoverlayHidden = true;

      var postdata = {
        channel: this.mySurveyChildOne,
        feedback_id: this.mySurvey,
        num_id: this.productNameId,
        third_level: this.lavelID,
        status: this.tickMarkSurvey,
        page_name: this.TabsName,
        device_id: this.deviceIs,
      }
      console.log(postdata);
      // this.shouldLeave = true;
      $.ajax({
        url: 'http://124.30.44.230/funaihelpver1/api/ver1/survey',
        method:'POST',
        crossDomain: true,
        beforeSend:function(xhr){
          xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        },
        async:true,
        data:postdata,
        success: function(result) {
          console.log(result);
          // $('.my-overlay').fadeOut();
         // that._location.back();
          // this._location.back();
        },
        error: function(request,msg,error) {
        }
      });

      setTimeout(()=>{
        $('.my-overlay').fadeOut();
        this.hidePopup = true;

        this.navCtrl.pop();
        this.shouldLeave = true;
      }, 2000);

    }

      justLeave()
      {
        console.log('justLeave called now');
        this.navCtrl.pop();
      }


      closePopUp(){
        $('.my-overlay').hide();
        this.navCtrl.pop();
        this.shouldLeave = true;
      }

      gotoBack()
      {
        this.navCtrl.pop();
      }

      pushRegister(){
        this.navCtrl.setRoot(RegisterPage);
      }

      setLang(val: any) {
      console.log(val);
      sessionStorage.setItem('lang', val);
      localStorage.setItem('lang', val);

      // this.navCtrl.setRoot(HomePage);

      this.currentCountry = this.langList.filter(c => {
        if (c.lgID == val) {
          return c;
        }
      });
      console.log(this.currentCountry[0].flag);
      this.countryClass = this.currentCountry[0].flag;
      sessionStorage.setItem('langClass', this.currentCountry[0].flag);
      //this.navCtrl.push(this.navCtrl.getActive().component);
    }



}
