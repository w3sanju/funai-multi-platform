import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ProductListProvider } from '../../providers/product-list/product-list';
import { RegisterPage } from './../register/register';

/**
 * Generated class for the VideosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html',
})
export class VideosPage {

  parent_Title3 = this.navParams.get('parent_Title2');
  new_title3 = this.navParams.get('new_title3');

  videoList: Array<{ v_path: any, v_poster: any, v_title: string }> = [] ;

  lang: any;
  langList: Array<{ lgName: string, lgID: number, flag: string }>;

  public langName: string = "US spanish";

    countryName: any;
    currentCountry;
    countryClass: any;


    selectOptions: any = {
      title: 'Language',
      mode: 'md',
      cssClass: 'my-class'
    };


  constructor(private productListProvider:ProductListProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideosPage');
  }

  ngOnInit(): void {

    if (!localStorage.getItem('lang')) {
      // sessionStorage.setItem('lang', '1');
      console.log('no lang');
      this.countryClass = 'us';
    }
    else {
      console.log('got lang');
    }


    this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
        { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
        { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
        { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
        { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

    if (sessionStorage.getItem("lang") !== null && sessionStorage.getItem("lang") !== "") {
      this.lang = sessionStorage.getItem('lang');
      this.countryName = sessionStorage.getItem('lang');
      this.countryClass = sessionStorage.getItem('langClass');
      console.log(this.countryName);
    }
    else {
      this.lang = sessionStorage.setItem('lang', '1');
      this.countryClass = 'us';
      sessionStorage.setItem('langClass','us');
      this.countryName = 1;
      console.log(this.countryName);
    }

    this.getVideoList();
  }

  getVideoList() {
    this.productListProvider.getProducts('videos.json?levelid='+localStorage.getItem('level_id3New')+'&numid='+localStorage.getItem('prod_num_id'))  .subscribe(success => {
        if (success) {
          console.log(success);
          for (var i = 0; i < success.length; i++) {
              this.videoList.push({v_path: 'http://www.funaihelp.com/media/uploads/videos/2/'+ success[i], v_poster: 'http://www.funaihelp.com/media/uploads/videos/2/'+ success[i].split('mp4').join('png'), v_title: success[i].split('mp4').join('').replace(/[^a-zA-Z ]/g, " ")});
          }
        } else {
          console.log('Error in success');
        }
      });
    }

    pushRegister(){
      this.navCtrl.setRoot(RegisterPage);
    }

    setLang(val: any) {
    console.log(val);
    sessionStorage.setItem('lang', val);
    localStorage.setItem('lang', val);

    // this.navCtrl.setRoot(HomePage);

    this.currentCountry = this.langList.filter(c => {
      if (c.lgID == val) {
        return c;
      }
    });
    console.log(this.currentCountry[0].flag);
    this.countryClass = this.currentCountry[0].flag;
    sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    //this.navCtrl.push(this.navCtrl.getActive().component);
  }


}
