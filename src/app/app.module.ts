import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import{ DownloadPage } from '../pages/download/download';
import{ TroubleshootPage } from '../pages/troubleshoot/troubleshoot';
import{ Troubleshoot_2Page } from '../pages/troubleshoot-2/troubleshoot-2';
import{ Troubleshoot_3Page } from '../pages/troubleshoot-3/troubleshoot-3';
import{ FaqsPage } from '../pages/faqs/faqs';

import { SettingsPage } from '../pages/settings/settings';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WeatherProvider } from '../providers/weather/weather';
import { ProductListProvider } from '../providers/product-list/product-list';

import { CategorySelectionPage } from '../pages/category-selection/category-selection';

import { FooterPage } from '../pages/shared/footer/footer.component';
import { RegisterPage } from '../pages/register/register';

import{ VideosPage } from '../pages/videos/videos';
import{ VchatPage } from '../pages/vchat/vchat';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DownloadPage,
    TroubleshootPage,
    Troubleshoot_2Page,
    Troubleshoot_3Page,
    FaqsPage,
    SettingsPage,
    CategorySelectionPage,
    FooterPage,
    RegisterPage,
    VideosPage,
    VchatPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DownloadPage,
    TroubleshootPage,
    Troubleshoot_2Page,
    Troubleshoot_3Page,
    FaqsPage,
    SettingsPage,
    CategorySelectionPage,
    FooterPage,
    RegisterPage,
    VideosPage,
    VchatPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WeatherProvider,
    ProductListProvider
  ]
})
export class AppModule {}
